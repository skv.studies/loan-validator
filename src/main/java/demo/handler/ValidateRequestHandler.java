package demo.handler;

import demo.utils.ProcessVariable;
import org.camunda.bpm.client.spring.annotation.ExternalTaskSubscription;
import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskHandler;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.context.annotation.Configuration;

@Configuration
@ExternalTaskSubscription("validateRequest")
public class ValidateRequestHandler implements ExternalTaskHandler {
    @Override
    public void execute(ExternalTask externalTask, ExternalTaskService externalTaskService) {
        Logger logger = LoggerFactory.getLogger(ValidateRequestHandler.class);
        logger.info("start");
        VariableMap variables = Variables.createVariables();
        if ((long) externalTask.getVariable(ProcessVariable.SUM) > 0 &&
                !externalTask.getVariable(ProcessVariable.GOAL).toString().isEmpty()) {
            variables.putValue("valid", true);
        } else {
            variables.putValue("valid", false);
        }
        logger.info("near end");
        externalTaskService.complete(externalTask, variables);
    }
}